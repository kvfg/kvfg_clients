# Tonausgabe an T480s

## Schnipsel

	#set card-profile to HDMI and Mic to internal Microphone
	pactl set-card-profile 0 output:hdmi-stereo+input:analog-stereo
	#set output to HDMI and adjust volume
	HDMISINK=$(pactl list short sinks | grep hdmi | awk '{print $1}')
	pacmd set-default-sink $HDMISINK
	pacmd set-sink-volume $HDMISINK 32768 



 ## Vorarbeiten

Lösungsansatz: https://askubuntu.com/questions/71863/how-to-change-pulseaudio-sink-with-pacmd-set-default-sink-during-playback

 Schnipsel für ein eigenes Skript

HDMI-Sink finden

	pactl list short sinks 

Gibt die Nummer des hdmi-sinks zurück 

	pactl list short sinks | grep hdmi | awk ‘{print $1}’ 

Gibt den Namen des hdmi-sinks-zurück 

	pactl list short sinks | grep hdmi | awk ‘{print $2}’ 

Auf HDMI-Sink umschalten

	pacmd set-default-sink 0 

Laufende Streams finden

	pacmd list-sink-inputs 

nur die IDs 

	pacmd list-sink-inputs | grep index: | awk ‘{print $2}’ 

die IDs in ein Array schreiben 

	mapfile -t activeStreams < <(pacmd list-sink-inputs | grep index: | awk ‘{print $2}’)

Laufdende Streams in HDMI-Sink verschieben 

	for ((i=0;i<${#activeStreams[@]};i++)); 
	do 
		pacmd move-sink-input 
		{activeStreams[$i]} $HDMISINK 
	done 

Lautstärke anpassen 

	pacmd set-sink-volume 0 16384 


