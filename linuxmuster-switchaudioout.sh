#!/bin/bash 

DEFAULTSINK=$(pactl info | grep Standard-Sink | awk ‘{print $2}’) 

# Falls das jemals nicht mehr funktioniert, dann 
# checken, was pactl info ausgibt... anpassen auf z.B. das: 

DEFAULTSINK=$(pactl info | grep Standard-Ziel | awk ‘{print $2}’) 

# $2 ist sink-Name, $1 waere sink-id 
HDMISINK=$(pactl list short sinks | grep hdmi | awk ‘{print $2}’) ANALOGSINK=$(pactl list short sinks | grep analog | awk ‘{print $2}’) 

if [ $DEFAULTSINK = $ANALOGSINK ] 
then 
	NEXTSINK=$HDMISINK NEXTSINKNAME=‘HDMI / DisplayPort’ # volume = 25% VOLUME=16384 
else
	NEXTSINK=$ANALOGSINK NEXTSINKNAME=‘Lautsprecher / Kopfhörer’ # volume = 75% VOLUME=49152 
fi 

# Set output to HDMI 

pacmd set-default-sink $NEXTSINK

# Find active streams
mapfile -t activeStreams < <(pacmd list-sink-inputs | grep index: | awk ‘{print $2}’)

# Move active streams to $NEXTSINK
for ((i=0;i<${#activeStreams[@]};i++)); 
do 
	pacmd move-sink-input ${activeStreams[$i]} $NEXTSINK 
done

# Set Volume to $VOLUME
pacmd set-sink-volume $NEXTSINK $VOLUME notify-send –expire-time=2000 “Tonausgabe” “Tonausgabe über $NEXTSINKNAME”

