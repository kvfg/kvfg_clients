#!/bin/bash 

HDMISINK=$(pactl list short sinks | grep hdmi | awk ‘{print $1}’) 

# Set output to HDMI 
pacmd set-default-sink $HDMISINK 

# Find active streams 
mapfile -t activeStreams < <(pacmd list-sink-inputs | grep index: | awk ‘{print $2}’) 

# Move active stream to HDMISINK 
for ((i=0;i<${#activeStreams[@]};i++)); do pacmd move-sink-input ${activeStreams[$i]} $HDMISINK done 

# Set Volume to ~25% 
pacmd set-sink-volume $HDMISINK 16384 notify-send “Lautstärke” “Volume 25% set” 

# Joplin Pic [linuxmuster-audio2hdmi](:/dc36087e351d4ed293418169678ced53)

